#include "../include/Wall.hpp"
#include <QGraphicsItem>
#include <QPainter>
#include <QStyleOption>
#include<QGraphicsView>

Wall::Wall(float x, float y, float width, float height)
    :m_x(x), m_y(y), m_height(height), m_width(width)
{
    setPos(m_x, m_y);
}

Wall::Wall(const Wall&) {
}

auto Wall::operator=(const Wall&) -> Wall {
    return *this;
}

void Wall::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {

    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setBrush(QColor(239, 239, 239));
    painter->setPen(Qt::NoPen);
    painter->drawRect(0, 0, m_width, m_height);
}

auto Wall::boundingRect() const -> QRectF {
    return QRectF(0, 0, m_width, m_height);
}

auto Wall::type() const -> int {
    return 0;
}

auto Wall::isVertical() const -> bool {
    return (m_height - m_width) > 0 ? true : false;
}

auto Wall::isHorizontal() const -> bool {
    return !isVertical();
}

auto Wall::getHeight() const -> float {
    return m_height;
}

auto Wall::getWidth() const -> float {
    return m_width;
}

auto Wall::getX() const -> float{
    return m_x;
}

auto Wall::getY() const -> float {
    return m_y;
}

auto Wall::getCoordinates() const -> std::pair<float,float> {
    return std::make_pair(m_x, m_y);
}
